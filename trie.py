from time import perf_counter
import random
import string

class Node:
    def __init__(self, char):
        self.char = char
        self.is_word = False
        self.children = [None] * 26
        
    def __str__(self):
        if self.children == [None] * 26:
            if self.is_word:
                return self.char.upper()
            return self.char
        
        children = []
        for child in self.children:
            if child is not None:
                children.append(child)

        children_str = ""
        for i in range(len(children)):
            if i == 0:
                children_str = str(children[i])
            else:
                children_str = children_str + ", " + str(children[i])
        if self.char == None:
            return children_str
        if self.is_word:
            return self.char.upper() + " -> " + "(" + children_str + ")"
        return self.char + " -> " + "(" + children_str + ")"


class Trie:
    def __init__(self):
        self.root = Node(None)
        
    def __str__(self):
        return self.root.__str__()
    
    def insert(self, word):
        node = self.root
        for char in word:
            if node.children[ord(char) - ord('a')] is None:
                node.children[ord(char) - ord('a')] = Node(char)
            node = node.children[ord(char) - ord('a')]
                
        node.is_word = True
        
    def exists(self, word):
        node = self.find_node(word)
        if node is None:
            return False
        return node.is_word
    
    def remove(self, word):
        node = self.root
        nq = [node]
        for char in word:
            if node.children[ord(char) - ord('a')] is None:
                return False
            node = node.children[ord(char) - ord('a')]
            nq.append(node)
        if not node.is_word:
            return False
        node.is_word = False
        while node:
            if node.children == [None] * 26 and not node.is_word:
                char = node.char
                try:
                    node = nq.pop()
                    node.children[ord(char) - ord('a')] = None
                except IndexError:
                    node = None
            else:
                return True
        return True
       
    def find_node(self, word):
        node = self.root
        for char in word:
            if node.children[ord(char) - ord('a')] is None:
                return None
            node = node.children[ord(char) - ord('a')]
        return node
    
    def find_by_prefix(self, prefix):
        result = []
        node = self.find_node(prefix)
        if node is None:
            return []
        children = []
        for child in node.children:
            if child is not None:
                children.append(child)
        try:
            child = children.pop()
        except IndexError:
            child = None
        while child:
            new_prefix = prefix + child.char
            result.extend(self.find_by_prefix(new_prefix))
            try:
                child = children.pop()
            except IndexError:
                child = None
        if node.is_word:
            result.append(prefix)
        return result
        
    
    @staticmethod
    def make_trie(words):
        trie = Trie()
        for word in words:
            trie.insert(word)
        return trie


def main():
    trie = Trie.make_trie(["cat", "car", "card", "cards", "carts", "trie", "try"])
    print("Trie struktuur: " + str(trie))
    print("Otsing prefix \"ca\" järgi: ")
    print(trie.find_by_prefix("ca"))
    print("Sõna \"cards\" eksisteerib: " + str(trie.exists("cards")))
    print("Sõna \"car\" eksisteerib: " + str(trie.exists("car")))

    print("Eemaldame sõnad car ja cards")
    trie.remove("car")
    trie.remove("cards")
    print("Trie struktuur: " + str(trie))
    print("Otsing prefix \"ca\" järgi: ")
    print(trie.find_by_prefix("ca"))
    print("Sõna \"cards\" eksisteerib: " + str(trie.exists("cards")))
    print("Sõna \"car\" eksisteerib: " + str(trie.exists("car")))

    # sample_sizes = [1000, 2000, 4000, 8000, 16000, 32000, 64000]
    # word_lengths = [10, 20]
    # for word_length in word_lengths:
    #     for size in sample_sizes:
    #         sum = 0
    #         for i in range(5):
    #             words = []
    #             for i in range(size):
    #                 words.append(''.join(random.choice(string.ascii_lowercase) for _ in range(word_length)))
    #             start = perf_counter()
    #             Trie.make_trie(words)
    #             millis = round(1000 * (perf_counter() - start))
    #             sum += millis
    #         print("Trie loomine võttis {} ms. sõna pikkus: {}, sõnade arv: {}".format(sum/5, word_length, size))


if __name__ == '__main__':
    main()
